/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import DB.MySQLconnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import javax.swing.JOptionPane;

/**
 *
 * @author Samuel
 */
public class Inscription {
    private String idCourse;    
    private String studentCode;    
    private String courseName;    
    private String studentName;    
    private String studentLastName;    
    private String amountPaid;    
    private String phone;    
    private boolean certificate;

    public String getIdCourse() {
        return idCourse;
    }

    public void setIdCourse(String idCourse) {
        this.idCourse = idCourse;
    }

    public String getStudentCode() {
        return studentCode;
    }

    public void setStudentCode(String studentCode) {
        this.studentCode = studentCode;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getStudentLastName() {
        return studentLastName;
    }

    public void setStudentLastName(String studentLastName) {
        this.studentLastName = studentLastName;
    }

    public String getAmountPaid() {
        return amountPaid;
    }

    public void setAmountPaid(String amountPaid) {
        this.amountPaid = amountPaid;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public boolean isCertificate() {
        return certificate;
    }

    public void setCertificate(boolean certificate) {
        this.certificate = certificate;
    }
    
   public void createInscription(String courseId,
                                 String studentCode){
        Course course = new Course();   
        Student student =  new Student();
        String SQLsentence= "";
        MySQLconnection mySql = new MySQLconnection();
        Connection connection = mySql.Connect();
        course = course.getCourseFromId(courseId);
        student = student.getStudentFromCode(studentCode);
        SQLsentence = "INSERT INTO inscripciones (id_curso,codigo_estudiante,nombre_curso, "
                + "nombre_estudiante, apellido_estudiante, monto_pagado, celular)"
                + "VALUES (?,?,?,?,?,?,?)";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(SQLsentence);
            preparedStatement.setString(1,courseId);
            preparedStatement.setString(2,studentCode);
            preparedStatement.setString(3,course.getCourseName());
            preparedStatement.setString(4,student.getName());
            preparedStatement.setString(5,student.getLastName());
            preparedStatement.setString(6,amountPaid = "2");
            preparedStatement.setString(7,student.getPhone());
            int i = preparedStatement.executeUpdate();
            if(i>0){
                JOptionPane.showMessageDialog(null, "Estudiante '"+student.getName()+" "+student.getLastName()+"' inscrito correctamente"
                        + " al curso '" +course.getCourseName()+"'");
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
   }
}
