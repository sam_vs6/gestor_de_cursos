
package Model;

import DB.MySQLconnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.JOptionPane;

public class Course {
    private String idCourse;    
    private String courseName;    
    private String startDate;    
    private String endDate;    
    private String startTime;    
    private String endTime;    

    public String getIdCourse() {
        return idCourse;
    }

    public void setIdCourse(String idCourse) {
        this.idCourse = idCourse;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }
    public void create(String courseName, 
                       String startDate,
                       String endDate, 
                       String startTime, 
                       String endTime) {
        String SQLsentence;
        MySQLconnection mySql = new MySQLconnection();
        Connection connection = mySql.Connect();
            SQLsentence = "INSERT INTO cursos (nombre_curso,fecha_inicio,fecha_fin, hora_inicio, hora_fin)"
                + "VALUES (?,?,?,?,?)";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(SQLsentence);
            preparedStatement.setString(1,courseName);
            preparedStatement.setString(2,startDate);
            preparedStatement.setString(3,endDate);
            preparedStatement.setString(4,startTime);
            preparedStatement.setString(5,endTime);
            int i = preparedStatement.executeUpdate();
            if(i>0){
                JOptionPane.showMessageDialog(null, "Curso "+courseName+" creado correctamente");
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
    }
    public ArrayList<Course> getAllCourses(){
        ArrayList<Course> results = new ArrayList();
        String SQLsentence= "";
        MySQLconnection mySql = new MySQLconnection();
        Connection connection = mySql.Connect();
        SQLsentence = "SELECT * FROM cursos ORDER BY id_curso";
        try {
            
            Statement statement= connection.createStatement();
            ResultSet resultSet = statement.executeQuery(SQLsentence);
            while(resultSet.next()){
                Course course = new Course();
                course.courseName = resultSet.getString("nombre_curso");
                course.idCourse = resultSet.getString("id_curso");
                course.startDate = resultSet.getString("fecha_inicio");
                course.endDate = resultSet.getString("fecha_fin");
                course.startTime = resultSet.getString("hora_inicio");
                course.endTime = resultSet.getString("hora_fin");
                results.add(course);
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
        return results;
    }
    public Course getCourseFromId(String idCourse){
        Course course = new Course();
        String SQLsentence= "";
        MySQLconnection mySql = new MySQLconnection();
        Connection connection = mySql.Connect();
        SQLsentence = "SELECT * FROM cursos WHERE id_curso = "+idCourse;
        try {
            Statement statement= connection.createStatement();
            ResultSet resultSet = statement.executeQuery(SQLsentence);
            while(resultSet.next()){
                course.courseName = resultSet.getString("nombre_curso");
                course.idCourse = resultSet.getString("id_curso");
                course.startDate = resultSet.getString("fecha_inicio");
                course.endDate = resultSet.getString("fecha_fin");
                course.startTime = resultSet.getString("hora_inicio");
                course.endTime = resultSet.getString("hora_fin");
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
        return course;
    }
    
    public void print(){
        System.out.print("---------------Curso-------"+"\n");
        System.out.print(idCourse+"\n");
        System.out.print(courseName+"\n");
        System.out.print(endDate+"\n");
        System.out.print(startDate+"\n");
    }
}
