
package Model;

import DB.MySQLconnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.JOptionPane;

public class Student {
    private String studentCode;
    private String ci;
    private String name;
    private String lastName;
    private String email;
    private String phone;

    public String getStudentCode() {
        return studentCode;
    }
    public void setStudentCode(String studentCode) {
        this.studentCode = studentCode;
    }
    public String getCi() {
        return ci;
    }
    public void setCi(String ci) {
        this.ci = ci;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getLastName() {
        return lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public String getPhone() {
        return phone;
    }
    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void create(String ci,
                       String name, 
                       String lastName,
                       String phone, 
                       String email) {
        String SQLsentence= "";
        MySQLconnection mySql = new MySQLconnection();
        Connection connection = mySql.Connect();
        SQLsentence = "INSERT INTO estudiantes (ci_estudiante,nombres,apellidos, celular, email)"
                + "VALUES (?,?,?,?,?)";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(SQLsentence);
            preparedStatement.setString(1,ci);
            preparedStatement.setString(2,name);
            preparedStatement.setString(3,lastName);
            preparedStatement.setString(4,phone);
            preparedStatement.setString(5,email);
            int i = preparedStatement.executeUpdate();
            if(i>0){
                JOptionPane.showMessageDialog(null, "Estudiante "+name+" "+lastName+" creado correctamente");
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
    }
    
    public ArrayList<Student> getAllStudents(){
        ArrayList<Student> results = new ArrayList();
        String SQLsentence= "";
        MySQLconnection mySql = new MySQLconnection();
        Connection connection = mySql.Connect();
        SQLsentence = "SELECT * FROM estudiantes ORDER BY codigo";
        try {
            
            Statement statement= connection.createStatement();
            ResultSet resultSet = statement.executeQuery(SQLsentence);
            while(resultSet.next()){
                Student student = new Student();
                student.ci = resultSet.getString("ci_estudiante");
                student.email = resultSet.getString("email");
                student.lastName = resultSet.getString("apellidos");
                student.name = resultSet.getString("nombres");
                student.studentCode = resultSet.getString("codigo");
                student.phone = resultSet.getString("celular");
                results.add(student);
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
        return results;
    }
    public Student getStudentFromCode(String studentCode){
        Student student = new Student();
        
        String SQLsentence= "";
        MySQLconnection mySql = new MySQLconnection();
        Connection connection = mySql.Connect();
        SQLsentence = "SELECT * FROM estudiantes WHERE codigo = "+studentCode;
        try {
            Statement statement= connection.createStatement();
            ResultSet resultSet = statement.executeQuery(SQLsentence);
            while(resultSet.next()){
                student.ci = resultSet.getString("ci_estudiante");
                student.email = resultSet.getString("email");
                student.lastName = resultSet.getString("apellidos");
                student.name = resultSet.getString("nombres");
                student.studentCode = resultSet.getString("codigo");
                student.phone = resultSet.getString("celular");
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
        student.print();
        return student;
    }
    
    public void print(){
        System.out.print("---------------Estudiante-------"+"\n");
        System.out.print(name+"\n");
        System.out.print(studentCode+"\n");
        System.out.print(lastName+"\n");
        System.out.print(email+"\n");
        System.out.print(phone+"\n");
    }
}
