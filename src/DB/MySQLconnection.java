/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DB;

import java.sql.Connection;
import java.sql.DriverManager;
import javax.swing.JOptionPane;

/**
 *
 * @author Samuel
 */
public class MySQLconnection {
    public String db= "cursos";
    public String url = "jdbc:mysql://192.168.1.10/"+db;
    public String user = "root";
    public String pass = "";
    public MySQLconnection(){}
    
    public Connection Connect(){
        Connection link = null;
        try{
            Class.forName("org.gjt.mm.mysql.Driver");
            link = DriverManager.getConnection(this.url, this.user, this.pass);
        }catch (Exception e){
            JOptionPane.showMessageDialog(null,e);
        }
        return link;
    }
}
